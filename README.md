# A web-base interface to generate led patterns

code for the coach : https://gitlab.forge.hefr.ch/samuel.baula/coachinterface

## A la racine :
- code : contient le code du frontend ,du backend et le script SQL de la DB
- docs : contient toute la documentation du projet

## code :
- app : contient le code du frontend
- backend : contient le code du backend
- docker_images : contient le script SQL de création des tables et l'image docker mysql
import React from 'react';
import '../App.css';
import { Redirect } from 'react-router-dom'
import { Box } from "@material-ui/core";
import { Button } from 'react-bootstrap';
import { withStyles } from '@material-ui/styles';
import PersonIcon from '@material-ui/icons/Person';
import { SketchPicker } from 'react-color';
import Rectangle from 'react-rectangle';
import { IconButton } from '@material-ui/core';
import NumericInput from 'react-numeric-input';
import { Stage, Layer, Rect, Text, Circle, Line } from 'react-konva';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Row, Col, Container } from 'react-bootstrap';
import TextField from '@material-ui/core/TextField';
import Select from 'react-select';
import ColorizeIcon from '@material-ui/icons/Colorize';

import AuthService from '..//services/AuthService';

import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Divider from '@material-ui/core/Divider'
import Collapse from '@material-ui/core/Collapse'

import IconExpandLess from '@material-ui/icons/ExpandLess'
import IconExpandMore from '@material-ui/icons/ExpandMore'
import IconDashboard from '@material-ui/icons/Dashboard'
import IconShoppingCart from '@material-ui/icons/ShoppingCart'
import IconPeople from '@material-ui/icons/People'
import IconBarChart from '@material-ui/icons/BarChart'
import IconLibraryBooks from '@material-ui/icons/LibraryBooks'

import { withAlert } from 'react-alert'

const useStyles = theme => ({
    LoginButton: {
        background: '#007aff',
        display: "flex",
        marginLeft: "92%",
        marginTop: "20px",
    }
});

class TestPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            apiResponse: "",
            logged: false,
            chosenColor: "#000000",
            displayColorPicker: false,
            imageSelected: 0,
            nbImages: 1,
            imageName: [],
            imageTime: [],
            LED_colors: [],
            getColor: false,
            marginPreviewButton: "0%",
            showDeleteErrorMessage: false,
            showSaveErrorMessage: false,
            showDeleteErrorMessage2: false,
            showSaveErrorMessage2: false,
            patternName: "",
            sound: "",
            category: "",
            open: [],
            selectedPatternToLoad: "",
            dataToLoad: [],
            dataToLoad2: [],
            showLoad: false,
        };
        this.emotionList = [
            { value: "Anger", label: "Anger" },
            { value: "Disgust", label: "Disgust" },
            { value: "Fear", label: "Fear" },
            { value: "Happiness", label: "Happiness" },
            { value: "Sadness", label: "Sadness" },
            { value: "Surprise", label: "Surprise" }
        ]
        this.soundList = [
            { value: "None", label: "None" },
            { value: "Anger", label: "Anger" },
            { value: "Disgust", label: "Disgust" },
            { value: "Fear", label: "Fear" },
            { value: "Happiness", label: "Happiness" },
            { value: "Sadness", label: "Sadness" },
            { value: "Surprise", label: "Surprise" }
        ]
        this.state.selectedSoud = { value: "None", label: "None" };
        this.state.selectedCategory = { value: "", label: "" };
        this.state.imageName[0] = "Image 1";
        this.state.imageTime[0] = 1;
        let table = [];
        this.changeSound("None");
        for (var i = 0; i < 18; i++) {
            table[i] = "#000000";
        }
        for (var i = 0; i < this.state.dataToLoad.length; i++) {
            this.state.open[i] = false;
        }
        this.state.LED_colors.push(table);
        this.uri = "http://localhost:9000/"
    }


    //test function
    callAPI() {
        fetch(this.uri + "test")
            .then(res => res.text())
            .then(res => this.setState({ apiResponse: res }));
    }

    //Function called when component is mounted
    componentWillMount() {
        //this.callAPI();
    }

    //function to logout
    logout = (e) => {
        e.preventDefault();
        AuthService.logOut();
        this.props.history.push('/');
    }

    //function to change the color selected to draw
    handleChangeComplete = (color) => {
        this.setState({ chosenColor: color.hex });
    };

    //function to show the color picker
    showColorPicker = () => {
        this.state.marginPreviewButton = "17%";
        this.setState({ displayColorPicker: true });
        //this.state.displayColorPicker = !this.state.displayColorPicker;
    }

    //function to hide the color picker
    hideColorPicker = () => {
        this.state.marginPreviewButton = "0%";
        this.setState({ displayColorPicker: false });
        //this.state.displayColorPicker = !this.state.displayColorPicker;
    }

    //function to change the name of the image
    onChange = (e) => {
        //this.setState({ imageName: e.target.value });
        this.state.imageName[this.state.imageSelected] = e.target.value;
        this.setState({ test: true });
    }

    //function to change the name of the pattern
    changeName = (e) => {
        this.setState({ patternName: e.target.value });
    }

    //function to set the get color to true
    getColor = () => {
        this.setState({ getColor: true });
        //this.state.getColor = true;
    }

    //function to update the led color
    updateLed = (e, id) => {
        console.log(id);
        //if we want to get the color, the chosen color will take the color from the led clicked
        if (this.state.getColor) {
            this.state.getColor = false;
            this.setState({ chosenColor: this.state.LED_colors[this.state.imageSelected][id] })
        }
        else {
            this.state.LED_colors[this.state.imageSelected][id] = this.state.chosenColor;
            this.setState({ test: true });
        }
    }

    //function to delete the current image
    deleteChosenImage = () => {
        if (this.state.nbImages == 1) {
            //cannot delete last image
            this.setState({ showDeleteErrorMessage: true });
            return;
        }
        this.state.imageName.splice(this.state.imageSelected, 1);
        this.state.LED_colors.splice(this.state.imageSelected, 1);
        this.state.imageTime.splice(this.state.imageSelected, 1);
        if (this.state.imageSelected == this.state.nbImages - 1) {
            this.state.imageSelected = this.state.imageSelected - 1;
        }
        this.setState({ nbImages: this.state.nbImages - 1 });

    }

    //function to add a new image
    /**
     * by default :
     *      every LED is black ("#000000")
     *      the name is "image " + the image number
     *      the time is set to 1s
    **/
    addNewImage = () => {
        let table = [];
        for (var i = 0; i < 18; i++) {
            table[i] = "#000000";
        }
        this.state.LED_colors.push(table);
        this.state.imageName[this.state.nbImages] = "Image " + (this.state.nbImages + 1);
        this.state.imageTime[this.state.nbImages] = 1;
        this.state.imageSelected = this.state.nbImages;
        this.setState({ nbImages: this.state.nbImages + 1 });
    }

    //function to change the selected image
    SelectedImage = (e, id) => {
        //console.log(id);
        //console.log(this.state.imageSelected);
        this.setState({ imageSelected: id });
        //console.log(this.state.imageSelected);
    }

    //function to change the time of the current image
    changeImageTime = (value) => {
        //check if input is smaller than 0.1
        if (value <= 0) {
            value = 0.1;
        }
        this.state.imageTime[this.state.imageSelected] = value;
        //console.log(this.state.imageTime[this.state.imageSelected]);
    }

    //function to change the category of the pattern
    changeCategory = (value) => {
        this.state.selectedCategory = { value: value, label: value };
        this.state.category = value;
        this.setState({ test: true });
    }

    //function to change the sound of the pattern
    changeSound = (value) => {
        this.state.selectedSoud = { value: value, label: value };
        this.state.sound = value;
        this.setState({ test: true });
    }

    //function to generate the code
    generateCode = () => {

    }

    showPatternToLoad = () => {
        this.state.showLoad = !this.state.showLoad

        if (this.state.showLoad) {
            fetch(this.uri + "patterns")
                .then(res => res.json())
                .then(res => {
                    this.state.dataToLoad = res;

                    for (var i = 0; i < this.state.dataToLoad.length; i++) {
                        this.state.open[i] = false;
                        var patterns = [];
                        for (var j = 1; j < this.state.dataToLoad[i].length; j++) {
                            patterns[j - 1] = this.state.dataToLoad[i][j];
                        }
                        this.state.dataToLoad2.push(patterns);
                    }

                    console.log(this.state.dataToLoad);

                    this.setState({ test: true });
                })
        }
        else {
            this.setState({ test: true });
        }
    }

    loadPattern = (value) => {
        this.state.selectedPatternToLoad = value;
        if (this.state.selectedPatternToLoad == "") return;
        fetch(this.uri + "patterns?pattern=" + this.state.selectedPatternToLoad)
            .then(res => res.json())
            .then(res => {
                this.state.patternName = res.patternName;
                this.state.imageName = res.images;
                this.state.imageTime = res.times;
                this.state.LED_colors = res.colors;
                this.state.imageSelected = 0;
                this.changeCategory(res.category);
                this.changeSound(res.sound);
                this.state.nbImages = this.state.imageName.length
                console.log(this.state.category);
                console.log(this.state.sound);
            })
    }

    //function to see the pattern on the coach
    preview = () => {
        if (this.state.patternName == "" || this.state.category == "" || this.state.sound == "") {
            //show error message
            this.setState({ showSaveErrorMessage: true });
            return;
        }
        for (var i = 0; i < this.state.nbImages; i++) {
            if (this.state.imageName[i] == "") {
                //show error message
                this.setState({ showSaveErrorMessage: true });
                return;
            }
        }
        //send data to the api
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ patternName: this.state.patternName, images: this.state.imageName, times: this.state.imageTime, colors: this.state.LED_colors, category: this.state.category, sound: this.state.sound })
        };
        fetch(this.uri + "setPattern", requestOptions)
            .then(res => res.text())
            .then(res => console.log(res));

        this.save();
    }

    //function to save the pattern
    save = () => {
        if (this.state.patternName == "" || this.state.category == "" || this.state.sound == "") {
            //show error message
            this.setState({ showSaveErrorMessage: true });
            return;
        }
        for (var i = 0; i < this.state.nbImages; i++) {
            if (this.state.imageName[i] == "") {
                //show error message
                this.setState({ showSaveErrorMessage: true });
                return;
            }
        }
        //send data to the api
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ patternName: this.state.patternName, images: this.state.imageName, times: this.state.imageTime, colors: this.state.LED_colors, category: this.state.category, sound: this.state.sound })
        };

        const alert = this.props.alert;

        fetch(this.uri + "save", requestOptions)
            .then(res => res.text())
            .then(res => {
                if (res.status === 200) {
                    //show data saved message
                    //alert.show('Data saved');
                }
                else {
                    //show error message
                    //alert.show('Error saving data');
                }
            });
    }

    handleClick = (key) => {
        //this.state.openAnger = !this.state.openAnger;
        this.state.open[key] = !this.state.open[key];
        this.setState({ test: true });
    }

    moveImageUp = () => {
        if (this.state.imageSelected == 0) {
            return;
        }
        var imageName = this.state.imageName[this.state.imageSelected];
        this.state.imageName[this.state.imageSelected] = this.state.imageName[this.state.imageSelected - 1];
        this.state.imageName[this.state.imageSelected - 1] = imageName;

        var colors = this.state.LED_colors[this.state.imageSelected];
        this.state.LED_colors[this.state.imageSelected] = this.state.LED_colors[this.state.imageSelected - 1];
        this.state.LED_colors[this.state.imageSelected - 1] = colors;

        var time = this.state.imageTime[this.state.imageSelected];
        this.state.imageTime[this.state.imageSelected] = this.state.imageTime[this.state.imageSelected - 1];
        this.state.imageTime[this.state.imageSelected - 1] = time;

        this.setState({ imageSelected: this.state.imageSelected - 1 });
    }

    moveImageDown = () => {
        if (this.state.imageSelected == this.state.nbImages - 1) {
            return;
        }
        var imageName = this.state.imageName[this.state.imageSelected];
        this.state.imageName[this.state.imageSelected] = this.state.imageName[this.state.imageSelected + 1];
        this.state.imageName[this.state.imageSelected + 1] = imageName;

        var colors = this.state.LED_colors[this.state.imageSelected];
        this.state.LED_colors[this.state.imageSelected] = this.state.LED_colors[this.state.imageSelected + 1];
        this.state.LED_colors[this.state.imageSelected + 1] = colors;

        var time = this.state.imageTime[this.state.imageSelected];
        this.state.imageTime[this.state.imageSelected] = this.state.imageTime[this.state.imageSelected + 1];
        this.state.imageTime[this.state.imageSelected + 1] = time;

        this.setState({ imageSelected: this.state.imageSelected + 1 });
    }

    newPattern = () => {
        this.state.displayColorPicker = false;
        this.state.imageSelected = 0;
        this.state.nbImages = 1;
        this.state.imageName = [];
        this.state.imageTime = [];
        this.state.LED_colors = [];
        this.state.getColor = false;
        this.state.marginPreviewButton = "0%";
        this.state.showDeleteErrorMessage = false;
        this.state.showSaveErrorMessage = false;
        this.state.showDeleteErrorMessage2 = false;
        this.state.showSaveErrorMessage2 = false;
        this.state.patternName = "";
        this.state.sound = "";
        this.state.category = "";
        this.state.open = [];
        this.state.selectedPatternToLoad = "";
        this.state.dataToLoad = [];
        this.state.dataToLoad2 = [];
        this.state.showLoad = false;

        this.state.selectedSoud = { value: "None", label: "None" };
        this.state.selectedCategory = { value: "", label: "" };
        this.state.imageName[0] = "Image 1";
        this.state.imageTime[0] = 1;
        let table = [];
        this.changeSound("None");
        for (var i = 0; i < 18; i++) {
            table[i] = "#000000";
        }
        this.state.LED_colors.push(table);
        for (var i = 0; i < this.state.dataToLoad.length; i++) {
            this.state.open[i] = false;
        }
    }


    render() {
        const { classes } = this.props;
        if (localStorage.getItem("userMail") == null || localStorage.getItem("userMail") == "{}")
            return <Redirect to="/login" />
        this.state.logged = true;
        if (this.state.showDeleteErrorMessage) {
            this.state.showDeleteErrorMessage = false;
            this.state.showDeleteErrorMessage2 = true;
        }
        else {
            this.state.showDeleteErrorMessage2 = false;
        }
        if (this.state.showSaveErrorMessage) {
            this.state.showSaveErrorMessage = false;
            this.state.showSaveErrorMessage2 = true;
        }
        else {
            this.state.showSaveErrorMessage2 = false;
        }
        return (
            <div className="MainPage">
                <header>
                    {this.state.logged ? <Box><Button variant="contained" className={classes.LoginButton} onClick={this.logout} style={{ color: "#ffffff" }}>Logout<PersonIcon /></Button> </Box> : <p></p>}
                </header>

                <Container style={{ marginLeft: 10 }}>
                    <Row md={3} lg={10} style={{ marginLeft: 10 }}>
                        <Col>Category :</Col>
                        <Col>Pattern name :</Col>
                    </Row>
                    <Row md={3} lg={10} style={{ marginLeft: 10 }}>
                        <Col style={{ marginTop: 20 }}>
                            <Select
                                defaultValue={this.state.imageName[0]}
                                label="Single select"
                                options={this.emotionList}
                                value={this.state.selectedCategory}
                                onChange={value => this.changeCategory(value.value)}
                            />
                        </Col>
                        <Col>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                width="50%"
                                id="patternName"
                                label="Pattern name"
                                name="patternName"
                                autoComplete="patternName"
                                autoFocus
                                value={this.state.patternName}
                                onChange={this.changeName}
                            />
                        </Col>
                        <Col>
                            <Button style={{ marginTop: 25, width: 120 }} onClick={this.newPattern}>New pattern</Button>
                            <Button style={{ marginTop: 25, width: 120, marginLeft: 40 }} onClick={this.showPatternToLoad}>Load</Button>
                            {this.state.showLoad ?
                                <div style={{ position: "absolute", height: 500, marginLeft: 300, marginTop: -100, zIndex: 10 }}>
                                    <List component="nav" className={classes.appMenu} disablePadding>
                                        {this.state.dataToLoad.map((item, key) =>
                                            <ListItem button onClick={e => this.handleClick(key)} className={classes.menuItem}>
                                                <ListItemIcon className={classes.menuItemIcon}>
                                                    <IconLibraryBooks />
                                                </ListItemIcon>
                                                <ListItemText primary={item[0]} />
                                                {this.state.open[key] ? <IconExpandLess /> : <IconExpandMore />}
                                                <Collapse in={this.state.open[key]} timeout="auto" unmountOnExit>
                                                    <Divider />
                                                    <List component="div" disablePadding>
                                                        {this.state.dataToLoad2[key].map((item2, key2) =>
                                                            <ListItem button className={classes.menuItem} onClick={(e) => this.loadPattern(item2)}>
                                                                <ListItemText inset primary={item2} />
                                                            </ListItem>
                                                        )}
                                                    </List>
                                                </Collapse>
                                            </ListItem>
                                        )}
                                    </List>
                                </div>
                                :
                                <p></p>
                            }
                        </Col>
                    </Row>
                </Container>
                <Container style={{ marginLeft: 20, height: 300 }}>
                    <Row md={3} lg={10} style={{ marginLeft: 10 }}>
                        <Col>
                            <Row style={{ marginTop: 20 }}>
                                <Box border={1} borderRadius={"5%"} p={1} height={600}>
                                    <Row style={{ marginBottom: 20 }}>
                                        <Col>
                                            <Button style={{ marginTop: 25, marginLeft: "20%", marginRight: 10, width: 50 }} onClick={this.moveImageDown}><IconExpandMore /></Button>
                                        </Col>
                                        <Col>
                                            <Button style={{ marginTop: 25, marginLeft: "20%", marginRight: 10, width: 50 }} onClick={this.addNewImage}>+</Button>
                                        </Col>
                                        <Col>
                                            <Button style={{ marginTop: 25, marginLeft: "20%", marginRight: 10, width: 50 }} onClick={this.moveImageUp}><IconExpandLess /></Button>
                                        </Col>
                                    </Row>
                                    <Box height={490} style={{ overflowY: "auto", overflowX: "hidden" }}>
                                        {this.state.imageName.map((item, key) =>
                                            <Row>
                                                {this.state.imageSelected == key ?
                                                    <Button key={this.state.nbImages} style={{ marginTop: 25, marginLeft: 20, marginRight: 20, width: 300, backgroundColor: "#00b8e6" }} onClick={(e) => this.SelectedImage(e, key)}>
                                                        {item}
                                                    </Button>
                                                    :
                                                    <Button key={this.state.nbImages} style={{ marginTop: 25, marginLeft: 20, marginRight: 20, width: 300 }} onClick={(e) => this.SelectedImage(e, key)}>
                                                        {item}
                                                    </Button>
                                                }
                                            </Row>
                                        )}
                                    </Box>
                                </Box>
                            </Row>
                        </Col>
                        <Col style={{ marginTop: 20 }}>
                            <Row>
                                Image name :
                            </Row>
                            <Row>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    width="50%"
                                    id="imageName"
                                    label="Image name"
                                    name="imageName"
                                    autoComplete="imageName"
                                    autoFocus
                                    value={this.state.imageName[this.state.imageSelected]}
                                    onChange={this.onChange}
                                />
                            </Row>
                        </Col>
                        <Col style={{ marginTop: 70, marginLeft: -100 }}>
                            <Button style={{ width: 120 }} onClick={this.deleteChosenImage}>Delete</Button>
                            <div style={{ color: 'red' }} hidden={!this.state.showDeleteErrorMessage2}>Cannot delete image when there is only 1</div>
                        </Col>
                    </Row>

                </Container>

                <Container style={{ marginLeft: 400 }}>
                    <Row>
                        <Col xs={7}>
                            <div style={{ marginTop: -160 }}>
                                <Stage width={window.innerWidth / 2} height={590}>
                                    <Layer>
                                        <Circle stroke="#000000" id="0" x={300} y={21} radius={20} fill={this.state.LED_colors[this.state.imageSelected][0]} onClick={(e) => this.updateLed(e, 0)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="1" x={400} y={21} radius={20} fill={this.state.LED_colors[this.state.imageSelected][1]} onClick={(e) => this.updateLed(e, 1)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="2" x={480} y={60} radius={20} fill={this.state.LED_colors[this.state.imageSelected][2]} onClick={(e) => this.updateLed(e, 2)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="3" x={550} y={120} radius={20} fill={this.state.LED_colors[this.state.imageSelected][3]} onClick={(e) => this.updateLed(e, 3)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="4" x={600} y={200} radius={20} fill={this.state.LED_colors[this.state.imageSelected][4]} onClick={(e) => this.updateLed(e, 4)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="5" x={620} y={290} radius={20} fill={this.state.LED_colors[this.state.imageSelected][5]} onClick={(e) => this.updateLed(e, 5)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="6" x={600} y={380} radius={20} fill={this.state.LED_colors[this.state.imageSelected][6]} onClick={(e) => this.updateLed(e, 6)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="7" x={550} y={460} radius={20} fill={this.state.LED_colors[this.state.imageSelected][7]} onClick={(e) => this.updateLed(e, 7)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="8" x={480} y={520} radius={20} fill={this.state.LED_colors[this.state.imageSelected][8]} onClick={(e) => this.updateLed(e, 8)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="9" x={400} y={560} radius={20} fill={this.state.LED_colors[this.state.imageSelected][9]} onClick={(e) => this.updateLed(e, 9)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="10" x={300} y={560} radius={20} fill={this.state.LED_colors[this.state.imageSelected][10]} onClick={(e) => this.updateLed(e, 10)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="11" x={220} y={520} radius={20} fill={this.state.LED_colors[this.state.imageSelected][11]} onClick={(e) => this.updateLed(e, 11)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="12" x={150} y={460} radius={20} fill={this.state.LED_colors[this.state.imageSelected][12]} onClick={(e) => this.updateLed(e, 12)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="13" x={100} y={380} radius={20} fill={this.state.LED_colors[this.state.imageSelected][13]} onClick={(e) => this.updateLed(e, 13)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="14" x={80} y={290} radius={20} fill={this.state.LED_colors[this.state.imageSelected][14]} onClick={(e) => this.updateLed(e, 14)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="15" x={100} y={200} radius={20} fill={this.state.LED_colors[this.state.imageSelected][15]} onClick={(e) => this.updateLed(e, 15)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="16" x={150} y={120} radius={20} fill={this.state.LED_colors[this.state.imageSelected][16]} onClick={(e) => this.updateLed(e, 16)} />
                                    </Layer>
                                    <Layer>
                                        <Circle stroke="#000000" id="17" x={220} y={60} radius={20} fill={this.state.LED_colors[this.state.imageSelected][17]} onClick={(e) => this.updateLed(e, 17)} />
                                    </Layer>
                                </Stage>
                            </div>
                        </Col>
                        <Col>
                            <Row>
                                <Col style={{ position: "absolute" }}>
                                    <Row>
                                        <div style={{ marginLeft: "30%", marginBottom: this.state.marginPreviewButton }}>
                                            <p>Color selected : </p>
                                            <Box border={1} p={2} padding={0}>
                                                <Rectangle aspectRatio={[5, 3]} style={{ width: '100%', height: '100%' }} >
                                                    <div onClick={this.showColorPicker} style={{ background: this.state.chosenColor, width: '100%', height: '100%', marginLeft: "0%", stroke: "#000000" }} />
                                                </Rectangle>
                                            </Box>
                                        </div>
                                        {this.state.displayColorPicker ?
                                            <div style={{ marginLeft: "17%", marginTop: "-15%", position: "absolute" }}>
                                                <SketchPicker
                                                    color={this.state.chosenColor}
                                                    onChangeComplete={this.handleChangeComplete}
                                                    disableAlpha={true}
                                                    style={{ zIndex: 1 }}
                                                />
                                                <center><Button style={{ width: "100px", height: "40px", marginLeft: "-5%" }} onClick={this.hideColorPicker}>Ok</Button></center>
                                            </div> : <p></p>
                                        }
                                    </Row>
                                    <Row style={{ marginBottom: "25%", marginLeft: "30%" }}>
                                        <IconButton aria-label="colorize" style={{ width: 80, height: 80 }} onClick={this.getColor} hidden={this.state.displayColorPicker}>
                                            <ColorizeIcon style={{ width: 50, height: 50 }} />
                                        </IconButton>
                                    </Row>
                                    <Row style={{ marginBottom: 20 }}>
                                        <Button style={{ width: 200, height: 50 }} onClick={this.preview} > Preview</Button>
                                        <div style={{ color: 'red', marginLeft: 240, position: "absolute" }} hidden={!this.state.showSaveErrorMessage2}>Cannot save when there is empty field</div>
                                    </Row>
                                    <Row>
                                        {false ? <Button style={{ width: 200, height: 50 }} onClick={this.generateCode} > Generate code</Button> : <p></p> /* button if we want to show the generated code */}
                                    </Row>
                                </Col>
                                <Col style={{ marginLeft: "80%", position: "absolute" }}>
                                    <Row>
                                        Image time (in seconds) :
                                    </Row>
                                    <Row style={{ marginLeft: "5%", marginTop: "5%" }}>
                                        <NumericInput min={0.1} step={0.1} size={5} value={this.state.imageTime[this.state.imageSelected]} onChange={value => this.changeImageTime(value)} />
                                    </Row>
                                    <Row style={{ marginTop: "10%" }}>
                                        Audio to play :
                                    </Row>
                                    <div style={{ marginTop: "5%", marginLeft: "-2%", width: 200 }}>
                                        <Select
                                            label="Single select"
                                            options={this.soundList}
                                            value={this.state.selectedSoud}
                                            onChange={value => this.changeSound(value.value)}
                                        />
                                    </div>
                                    <Row>

                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>
                <footer>
                    <Button style={{ marginLeft: "80%", marginTop: "-5%", width: 200, height: 60 }} onClick={this.save} > Save</Button>
                </footer>
            </div>
        );
    }
}

export default withStyles(useStyles)(TestPage);
import React from 'react';
import logo from '../logo.svg';
import '../App.css';
import { Redirect } from 'react-router-dom'
import { Box } from "@material-ui/core";
import { Button } from 'react-bootstrap';
import { withStyles } from '@material-ui/styles';
import PersonIcon from '@material-ui/icons/Person';

import AuthService from '..//services/AuthService';

const useStyles = theme => ({
    LoginButton: {
        background: '#FFFFFF',
    }
});

class TestPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            apiResponse: "",
            logged: false,
        };
        this.uri = "http://localhost:9000/test"
    }


    callAPI() {
        fetch(this.uri)
            .then(res => res.text())
            .then(res => this.setState({ apiResponse: res }));
    }

    componentWillMount() {
        this.callAPI();
    }

    logout = (e) => {
        e.preventDefault();
        AuthService.logOut();
        this.props.history.push('/');
    }

    render() {
        const { classes } = this.props;
        if (localStorage.getItem("userMail") == null || localStorage.getItem("userMail") == "{}")
            return <Redirect to="/login" />
        this.state.logged = true;
        return (
            <div className="TestPage">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <p>
                        Edit <code>src/App.js</code> and save to reload.
        </p>
                    <a
                        className="App-link"
                        href="https://reactjs.org"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Learn React
        </a>
                </header>
                <center>
                    <p>{this.state.apiResponse}</p>
                    {this.state.logged ? <Box><Button variant="contained" className={classes.LoginButton} onClick={this.logout}>Logout<PersonIcon /></Button> </Box>:<p></p>}
                </center>
            </div>
        );
    }
}

export default withStyles(useStyles)(TestPage);
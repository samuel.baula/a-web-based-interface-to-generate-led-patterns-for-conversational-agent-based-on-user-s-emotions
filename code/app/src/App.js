import * as React from 'react';
import { createMuiTheme } from '@material-ui/core';
import { darken, lighten, ThemeProvider } from '@material-ui/core/styles';
import { blue, lightBlue } from '@material-ui/core/colors';
import Login from './components/Login';
import TestPage from './components/TestPage';
import MainPage from './components/MainPage';

import {
    BrowserRouter,
    Switch,
    Route
} from "react-router-dom";

const theme = createMuiTheme({
    palette: {
        primary: blue,
        secondary: {
            light: lighten(blue.A400, 0.07),
            main: blue.A400,
            dark: darken(blue.A400, 0.1)
        },
    },
});

export default () => (
    <BrowserRouter>
        <ThemeProvider theme={theme}>
            <Switch>
                <Route path="/login" component={Login} />
                <Route path="/test" component={TestPage} />
                <Route path="/" component={MainPage} />
            </Switch>
        </ThemeProvider>
    </BrowserRouter>
);
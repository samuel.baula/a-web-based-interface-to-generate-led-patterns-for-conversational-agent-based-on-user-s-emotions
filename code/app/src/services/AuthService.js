import axios from 'axios';

const USER_API_BASE_URL = 'http://localhost:9000';

class AuthService {

    login(credentials) {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: credentials
        };
        console.log("test");
        return fetch(USER_API_BASE_URL + "/login", requestOptions);
    }

    getUserInfo(){
        return JSON.parse(localStorage.getItem("userInfo"));
    }

    getUserMail() {
        return JSON.parse(localStorage.getItem("userMail"));
    }

    getAuthHeader() {
        if (this.getUserInfo() != null) {
            console.log(this.getUserInfo().token);
            return { headers: { Authorization: 'Bearer ' + this.getUserInfo().token } };
        } else {
            return null;
        }
        
    }

    async verify() {
        return await axios.get(USER_API_BASE_URL + "/verify", this.getAuthHeader());
    }

    logOut() {
        localStorage.removeItem("userInfo");
        localStorage.removeItem("userMail");
        //return axios.post(USER_API_BASE_URL + 'logout', {}, this.getAuthHeader());
    }
}

export default new AuthService();
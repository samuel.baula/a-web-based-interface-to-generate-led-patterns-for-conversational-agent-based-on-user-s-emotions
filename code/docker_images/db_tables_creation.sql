use nestore;

CREATE TABLE Admin(
   Username VARCHAR(50),
   Password VARCHAR(100) NOT NULL,
   PRIMARY KEY(Username)
);

insert into Admin values ('nestore', 'Nestore');


CREATE TABLE Category(
   Category_name VARCHAR(100),
   PRIMARY KEY(Category_name)
);

insert into Category values ('Anger');
insert into Category values ('Disgust');
insert into Category values ('Fear');
insert into Category values ('Happiness');
insert into Category values ('Sadness');
insert into Category values ('Surprise');


CREATE TABLE Pattern(
   Pattern_name VARCHAR(50),
   Audio VARCHAR(100) NOT NULL,
   Category_name VARCHAR(100) NOT NULL,
   PRIMARY KEY(Pattern_name),
   FOREIGN KEY(Category_name) REFERENCES Category(Category_name)
);

CREATE TABLE Image(
   Id_Image int AUTO_INCREMENT,
   Image_name VARCHAR(50) NOT NULL,
   Image_number INT NOT NULL,
   Image_time DECIMAL(4,2) NOT NULL,
   Color_led1 VARCHAR(7) NOT NULL,
   Color_led2 VARCHAR(7) NOT NULL,
   Color_led3 VARCHAR(7) NOT NULL,
   Color_led4 VARCHAR(7) NOT NULL,
   Color_led5 VARCHAR(7) NOT NULL,
   Color_led6 VARCHAR(7) NOT NULL,
   Color_led7 VARCHAR(7) NOT NULL,
   Color_led8 VARCHAR(7) NOT NULL,
   Color_led9 VARCHAR(7) NOT NULL,
   Color_led10 VARCHAR(7) NOT NULL,
   Color_led11 VARCHAR(7) NOT NULL,
   Color_led12 VARCHAR(7) NOT NULL,
   Color_led13 VARCHAR(7) NOT NULL,
   Color_led14 VARCHAR(7) NOT NULL,
   Color_led15 VARCHAR(7) NOT NULL,
   Color_led16 VARCHAR(7) NOT NULL,
   Color_led17 VARCHAR(7) NOT NULL,
   Color_led18 VARCHAR(7) NOT NULL,
   Pattern_name VARCHAR(50) NOT NULL,
   PRIMARY KEY(Id_Image),
   FOREIGN KEY(Pattern_name) REFERENCES Pattern(Pattern_name)
);

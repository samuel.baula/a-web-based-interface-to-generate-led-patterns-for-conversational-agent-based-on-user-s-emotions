var express = require('express');
var router = express.Router();

const axios = require('axios');

var pattern;
var getPattern = false;


/* GET pattern */
router.get('/', function (req, res, next) {
    if (getPattern) {
        res.setHeader('Content-Type', 'application/json');
        res.status(200);
        //TO DO
        res.end(JSON.stringify(pattern));
        //getPattern = false;
    }
    else {
        res.setHeader('Content-Type', 'application/json');
        res.status(500);
        //TO DO
        res.end("No pattern to get");
    }
});

/* POST : set pattern to get */
router.post('/', function (req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    res.status(200);
    /*console.log(req.body.patternName);
    console.log(req.body.images);
    console.log(req.body.times);
    console.log(req.body.colors);
    console.log(req.body.category);
    console.log(req.body.sound);*/
    res.end("Data saved");

    //TO DO check informations validity


    //TO DO set return pattern
    pattern = req.body;

    getAudioString();
    generateCode();

    getPattern = true;
    console.log(pattern);
});

//Code from https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function getAudioString() {
    if(pattern.sound != "None"){
        var access_token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJWN0FXSW1qT2lRdi13dElwTjk2MWJfY004amgzZ21OblZtMHVNVVUwQldrIn0.eyJqdGkiOiJjNGZjNGM3ZC1mNmE3LTQ2MzQtOTg2OS02OTEzNWQxOThhNDYiLCJleHAiOjE1OTQ4ODQyOTAsIm5iZiI6MCwiaWF0IjoxNTkyMjkyMjkwLCJpc3MiOiJodHRwczovL2lhbS5uZXN0b3JlLWNvYWNoLmV1L2F1dGgvcmVhbG1zL25lc3RvcmUtY29hY2giLCJhdWQiOiJtb2JpbGV2MSIsInN1YiI6ImY1YjlhYmI4LWJjZWEtNDRhNC1hYzk1LWRhNmFmYjAzMTgyYSIsInR5cCI6IkJlYXJlciIsImF6cCI6Im1vYmlsZXYxIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiNzlmMDdkNzgtYTI3MC00ODJiLWJhMTEtZjNlNjc5M2E4NjQ3IiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsIm5hbWUiOiJEaWRpZXIgQ3JhdXNheiIsInByZWZlcnJlZF91c2VybmFtZSI6ImRpZGllci5jcmF1c2F6QGhlZnIuY2giLCJnaXZlbl9uYW1lIjoiRGlkaWVyIiwibG9jYWxlIjoiaXQiLCJmYW1pbHlfbmFtZSI6IkNyYXVzYXoiLCJlbWFpbCI6ImRpZGllci5jcmF1c2F6QGhlZnIuY2gifQ.BlCcr16Qku4vNE3ffpfUM-RIEDC71s78xvotMrVm8r9xHSh3s-FWcX0YJPYSUzU23-gzsVmYdAdXIt_NAF8ddyDl-Tzqr03m-mhPGFOFwxo4-TFkyG_hKiA66bxodS3jGr62-JIXYzXjYu7pM6UWow9qGdQ2ZWR32iwkNFC29j2zAmSXd0Jt8Mt_8Sw4nTZll5t-UuF5_6EmbrRVbMV_oBJriPsE6kiOC_tKyXlNE-IMaVPEerPfV6kTawmZaPj-1eQ9Lfy0A0WNzTjvhy64wBX0obkuBR1WjB1xVS4sUO74wbHsI-K89XaSjvNpvFwN-IJ3fj1BA4VsyeVK0Zdp_A";
        axios.post('https://api.nestore-coach.eu/prod/hesso/coach/chatbot/tangible/message?lang=en', {
            text: "end_of_day"
          },{
            headers: {
            'Authorization': 'Bearer ' + access_token
            }
          })
          .then((res) => {
            console.log(res.data.text);

            //send feelings to get text to play as audio on the coach
            console.log(pattern);
            var soundCategory = pattern.sound;
            var emotion = "";

            switch(soundCategory){
                case "Anger":
                    emotion = "angry";
                    break;
                case "Disgust":
                    emotion = "disgusted";
                    break;
                case "Fear":
                    emotion = "scared";
                    break;
                case "Happiness":
                    emotion = "happy";
                    break;
                case "Sadness":
                    emotion = "sad";
                    break;
                case "Surprise":
                    emotion = "surprised";
                    break;
                default:
                    emotion = "";
            }

            axios.post('https://api.nestore-coach.eu/prod/hesso/coach/chatbot/tangible/message?lang=en', {
                text: "i am feeling "+emotion
            },{
                headers: {
                'Authorization': 'Bearer ' + access_token
                }
            })
            .then((res) => {
                console.log(res.data.text);
                pattern.sound = res.data.text;
            })
            .catch((error) => {
                console.error(error);
            })
          })
          .catch((error) => {
            console.error(error);
          })
    }
}

function generateCode() {
    //initialisation
    pattern.code = "from matrix_lite import led\nimport time\nfrom time import sleep\n#init\neverloop = ['black'] * led.length\n";
    
    for (var i = 0; i < 18; i++) {
        var rgb = hexToRgb(pattern.colors[0][i]);
        pattern.code += "everloop[" + i + "] = {'r':" + rgb.r + ", 'g':" + rgb.g + ", 'b':" + rgb.b + ", 'w':0 }\n";
    }
    pattern.code += "led.set(everloop)\ni = 0\n";

    //function
    pattern.code += "def execute_pattern():\n";
    pattern.code += "\tglobal i\n";
    for (var i = 0; i < pattern.colors.length; i++) {
        pattern.code += "\tif i == " + i + " :\n";
        for (var j = 0; j < 18; j++) {
            var rgb;
            if (j == 17) {
                rgb = hexToRgb(pattern.colors[i][0]);
            }
            else {
                rgb = hexToRgb(pattern.colors[i][j+1]);
            }
            pattern.code += "\t\teverloop[" + j + "] = {'r':" + rgb.r + ", 'g':" + rgb.g + ", 'b':" + rgb.b + ", 'w':0 }\n";
            pattern.code += "\t\tled.set(everloop)\n";
        }
        if (pattern.colors.length == i - 1) {
            pattern.code += "\t\t i = 0\n";
        }
        else {
            pattern.code += "\t\ti += 1\n";
        }
        pattern.code += "\t\tsleep(" + pattern.times[i] + ")\n";
    }
    pattern.code += "\tif i == " + pattern.times.length + " :\n";
    pattern.code += "\t\ti = 0\n";
}


module.exports = router;

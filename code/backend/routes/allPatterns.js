var express = require('express');
var router = express.Router();

var mysql = require('mysql');

var patternToLoad = {};

var con = mysql.createConnection({
    host: "160.98.47.126",
    //host: "localhost",
    user: "root",
    password: "nestore2018",
    database: "nestore"
});

var connection = false;

function connect() {
    if (!connection) {
        con.connect(function (err) {
            if (err) {
                console.log(err);
                return;
            }
            connection = true;
        });
    }
}

/* GET pattern */
router.get('/', function (req, res, next) {
    var pattern = req.query.pattern;

    connect();

    if (pattern == undefined) {

        con.query("SELECT pattern_name, category_name FROM Pattern;", function (err, result, fields) {
            if (err) throw err;

            console.log(result);
            var patterns = [];
            var categories = [];

            for (var i = 0; i < result.length; i++) {
                patterns[i] = result[i].pattern_name;
                categories[i] = result[i].category_name;
            }


            var nbCat = categories.length;
            for (var i = 0; i < categories.length; i++) {
                for (var j = i + 1; j < categories.length; j++) {
                    if (categories[i] == categories[j]) {
                        nbCat--;
                        break;
                    }
                }
            }

            var patternsPerCategory = new Array(nbCat);
            /*for (var i = 0; i < patternsPerCategory.length; i++) {
                patternsPerCategory[i] = new Array(result.length+1);
                for (var j = 0; j < result.length+1; j++) {
                    patternsPerCategory[i][j] = "";
                }
            }*/




            for (var i = 0; i < result.length; i++) {
                var add = true;
                var j = 0;
                for (j = 0; j < patternsPerCategory.length; j++) {
                    if (patternsPerCategory[j] == undefined) break;
                    if (patternsPerCategory[j][0] == result[i].category_name) {
                        add = false;
                        /*for (var k = 2; k < patternsPerCategory[j].length; k++) {
                            if (patternsPerCategory[j][k] == "") {
                                patternsPerCategory[j][k] = result[i].pattern_name;
                                break;
                            }
                        }*/
                    }
                }
                if (add) {
                    var nbPatternsInCat = 1;
                    for (var k = i+1; k < result.length; k++) {
                        if (result[i].category_name == result[k].category_name) nbPatternsInCat++;
                    }
                    patternsPerCategory[j] = new Array(nbPatternsInCat+1);
                    patternsPerCategory[j][0] = result[i].category_name;
                    //console.log(result[i].category_name);
                    //console.log(nbPatternsInCat);
                    //patternsPerCategory[index][1] = result[i].pattern_name;
                }
            }

            for (var i = 0; i < result.length; i++) {
                for (var j = 0; j < patternsPerCategory.length; j++) {
                    if (result[i].category_name == patternsPerCategory[j][0]) {
                        for (var k = 1; k < patternsPerCategory[j].length; k++) {
                            if (patternsPerCategory[j][k] == undefined) {
                                patternsPerCategory[j][k] = result[i].pattern_name;
                                break;
                            }
                        }
                    }
                }
            }

            /*let patternsPerCategory = {
                category: "",
                patterns: []
            }
            var patternsPerCategoryTable = [];

            for (var i = 0; i < result.length; i++) {
                var add = true;
                for (var j = 0; j < patternsPerCategoryTable.length; j++) {
                    console.log("Cat name :")
                    console.log(result[i].category_name)
                    if (patternsPerCategoryTable[j].category === result[i].category_name) {
                        add = false;
                        patternsPerCategoryTable[j].patterns.push(result[i].pattern_name);
                    }
                }
                if (add) {
                    patternsPerCategory.category = result[i].category_name;
                    patternsPerCategory.patterns.push(result[i].pattern_name);
                    patternsPerCategoryTable.push(patternsPerCategory);
                }
            }*/


            allPatterns = {
                patterns: patterns,
                categories: categories
            }

            res.setHeader('Content-Type', 'application/json');
            res.status(200);
            res.end(JSON.stringify(patternsPerCategory));
        });

        return;
    }

    con.query("SELECT * FROM Pattern NATURAL JOIN Image where pattern_name='" + pattern + "' order by Image_number;", function (err, result, fields) {
        if (err) {
            console.log(err);
        }
        console.log(result);
        console.log(result.length);
        console.log(result[0]);
        console.log(result[0].Pattern_name);

        var patternName = result[0].Pattern_name;
        var images = [];
        var times = [];
        var colors = [];
        for (var i = 0; i < result.length; i++) {
            images[i] = result[i].Image_name;
            times[i] = result[i].Image_time;
            colors[i] = new Array(18);
            colors[i][0] = result[i].Color_led1;
            colors[i][1] = result[i].Color_led2;
            colors[i][2] = result[i].Color_led3;
            colors[i][3] = result[i].Color_led4;
            colors[i][4] = result[i].Color_led5;
            colors[i][5] = result[i].Color_led6;
            colors[i][6] = result[i].Color_led7;
            colors[i][7] = result[i].Color_led8;
            colors[i][8] = result[i].Color_led9;
            colors[i][9] = result[i].Color_led10;
            colors[i][10] = result[i].Color_led11;
            colors[i][11] = result[i].Color_led12;
            colors[i][12] = result[i].Color_led13;
            colors[i][13] = result[i].Color_led14;
            colors[i][14] = result[i].Color_led15;
            colors[i][15] = result[i].Color_led16;
            colors[i][16] = result[i].Color_led17;
            colors[i][17] = result[i].Color_led18;
        }
        console.log(colors);
        var category = result[0].Category_name;
        var sound = result[0].Audio;

        patternToLoad = {
            patternName: patternName,
            images: images,
            times: times,
            colors: colors,
            category: category,
            sound: sound
        }

        res.setHeader('Content-Type', 'application/json');
        res.status(200);

        res.end(JSON.stringify(patternToLoad));

    });


});


module.exports = router;